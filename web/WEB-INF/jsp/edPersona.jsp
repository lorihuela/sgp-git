<%-- 
    Document   : editar
    Created on : 12/04/2019, 06:53:44 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Editar Cliente</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">   
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Editar Persona</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Nombres</label>
                        <input type="text" name="nombres" class="form-control" value="${lista[0].nombres}">
                        <label>Apellidos</label>
                        <input type="text" name="apellidos" class="form-control" value="${lista[0].apellidos}">
                        <label>Tipo Documento</label>
                        <input type="text" name="tipo_documento" class="form-control" value="${lista[0].tipo_documento}">
                        <label>Nro.Documento</label>
                        <input type="text" name="nro_documento" class="form-control" value="${lista[0].nro_documento}">
                        <label>Tipo Persona</label>
                        <input type="text" name="tipo_persona" class="form-control" value="${lista[0].tipo_persona}">
                        <label>Direccion</label>
                        <input type="text" name="direccion" class="form-control" value="${lista[0].direccion}">
                        <label>Teléfono</label>
                        <input type="text" name="telefono" class="form-control" value="${lista[0].telefono}">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" value="${lista[0].email}">
                        <label>Web</label>
                        <input type="text" name="web" class="form-control" value="${lista[0].web}">
                        <input type="submit" value="Confirmar" name="agEdConfirmar" class="btn btn-success">
                        <a href="index.htm"/>Regresar
                    </form>            
                </div>
            </div>            
        </div>   
    </body>
</html>
