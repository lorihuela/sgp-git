<%-- 
    Document   : editar
    Created on : 12/04/2019, 06:53:44 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Editar Cliente</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">   
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Editar Usuario</h4>
                </div>
                <div class="card-body">
                    <form method="POST">
                        <label>Nick</label>
                        <input type="text" name="nick" class="form-control" value="${lista[0].nick}">
                        <label>Contraseña</label>
                        <input type="password" name="contrasenia" class="form-control" value="${lista[0].contrasenia}">
                        <label>Cod.Persona</label>
                        <input type="text" name="idpersona" class="form-control" value="${lista[0].idpersona}">
                        <label>Cod.Rol</label>
                        <input type="text" name="idrol" class="form-control" value="${lista[0].idrol}">                        
                        <input type="submit" value="Confirmar" name="agEdConfirmar" class="btn btn-success">
                        <a href="liUsuario.htm"/>Regresar
                    </form>            
                </div>
            </div>            
        </div>   
    </body>
</html>
