<%-- 
    Document   : index
    Created on : 13/04/2019, 02:59:32 PM
    Author     : user
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>LOGIN SGP</title>
    </head>
    <body>
        <div class="container col-lg-3">
            <form method="POST">
                <div class="form-group mt-4">
                    <img src="../../icons/user_login.png" hidden="80" width="80"/>
                    <p><strong>SGP LOGIN</strong></p>                    
                </div>
                <div class="form-group">
                    <label>Usuario</label>
                    <input class="form-control" type="text" name="usuario" placeholder="Nick"/>
                </div>
                <div class="form-group">
                    <label>Contraseña</label>
                    <input type="text" class="form-control" name="contrasenia" placeholder="************"/>
                </div>
                <div>
                    <input class="btn btn-danger btn-block" type="submit" value="Ingresar" name="ingresar">
                </div>
            </form>
        </div>                       
    </body>
</html>
