<%-- 
    Document   : editar
    Created on : 12/04/2019, 06:53:44 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>SGP</title>
    </head>
    <body>
        <div class="container mt-4 col-lg-4">   
            <div class="card border-info">
                <div class="card-header bg-info">
                    <h4>Editar US</h4>
                </div>
                <div class="card-body">
                    <form method="POST">                
                        <label>Nombre corto</label>
                        <input type="text" name="nombre_corto" class="form-control" value="${lista[0].nombre_corto}">
                        <label>Nombre largo</label>
                        <input type="text" name="nombre_largo" class="form-control" value="${lista[0].nombre_largo}">
                        <label>Descripción</label>
                        <input type="text" name="descripcion" class="form-control" value="${lista[0].descripcion}">
                        <label>Valor de negocio</label>
                        <input type="text" name="valor_negocio" class="form-control" value="${lista[0].valor_negocio}">
                        <label>Valor Técnico</label>
                        <input type="text" name="valor_tecnico" class="form-control" value="${lista[0].valor_tecnico}">
                        <label>Nota</label>
                        <input type="text" name="nota" class="form-control" value="${lista[0].nota}">
                        <label>Tiempo de Desarrollo</label>
                        <input type="text" name="tiempo_desarrollo" class="form-control" value="${lista[0].tiempo_desarrollo}">
                        <label>COD.Proyecto</label>
                        <input type="text" name="idProyecto" class="form-control" value="${lista[0].idproyecto}">
                        <label>COD.Equipo</label>
                        <input type="text" name="idProyecto_equipo" class="form-control" value="${lista[0].idproyecto_equipo}">
                        <label>Kanban</label>
                        <input type="text" name="idKanban" class="form-control" value="${lista[0].idkanban}">
                        <label>COD.Sprint</label>
                        <input type="text" name="idProyecto_sprint" class="form-control" value="${lista[0].idproyecto_sprint}">
                        <input type="submit" value="Confirmar" name="agEdConfirmar" class="btn btn-success">
                        <a href="index.htm"/>Regresar
                    </form>            
                </div>
            </div>            
        </div>   
    </body>
</html>
