<%-- 
    Document   : liPersona
    Created on : 13/04/2019, 04:06:19 PM
    Author     : user
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>SGP</title>
    </head>
    <body>
        <div class="card-header bg-info text-white">
            <h3 align="center">HISTORIA DE USUARIOS</h3>
        </div>
        <div class="container mt-4">
            <div class="card border-info">
                <div class="card-header bg-info text-white">
                    <a class="btn btn-light" href="agProyecto_us.htm">Agregar</a>
                </div>
                
                <div class="card-body">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>VN</th>
                                <th>VT</th>
                                <th>TD(Dias)</th>
                                <th>Estado</th>
                                <th>COD.Proy.</th>
                                <th>ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="dato" items="${lista}">
                            <tr>
                                <td>${dato.idproyecto_us}</td>
                                <td>${dato.nombre_corto}</td>                                
                                <td>${dato.descripcion}</td>
                                <td>${dato.valor_negocio}</td>
                                <td>${dato.valor_tecnico}</td>
                                <td>${dato.tiempo_desarrollo}</td>
                                <td>${dato.estado}</td>
                                <td>${dato.cod_proyecto}</td>
                                <td>
                                    <a href="edProyecto_us.htm?id=${dato.idproyecto_us}" class="btn btn-warning">Editar</a>
                                    <a href="elProyect_us.htm?id=${dato.idproyecto_us}" class="btn btn-danger">Eliminar</a>
                                </td>
                            </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>                
            </div>
        </div>
    </body>
</html>
